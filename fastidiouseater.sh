#!/bin/sh
# Counts the number of ways one can consume a plate of
# n french fries, eating only 1 or 2 at a time.
fastidiousEaterHelper() {
  # let F' = F^-1
  j=0
  j_1=1
  k=0
  k_1=0
  while [ $# -gt 0 ]; do # invariant: k_1 <= F(n)
    if [ $1 -eq 0 ]; then
      # k=F(2F'(j)); k_1=F(2F'(j)+1)
      k=`expr $j '*' '(' 2 '*' $j_1 - $j ')'`
      k_1=`expr $j '*' $j + $j_1 '*' $j_1`
      j=$k
      j_1=$k_1
      shift
    else
      # k=F(2F'(j_1)-1); k_1=F(2F'(j_1))
      k=`expr $j_1 '*' $j_1 + $j '*' $j`
      k_1=`expr $j_1 '*' '(' 2 '*' $j + $j_1 ')'`
      j=$k
      j_1=$k_1
      shift
    fi
  done
  echo $k_1
}

fastidiousEater() {
  [ -z $1 ] && exit
  case $1 in
    *[!0-9]*) return 1;;
  esac
  i=$1
  shift
  while [ $i -ge 1 ]; do # invariant: reversing every operation in $*
                         #            left-to-right on i gives n
    if [ `expr $i % 2` -eq 0 ]; then
      i=`expr $i / 2`
      set 0 $*
    else
      i=`expr '(' $i - 1 ')' / 2`
      set 1 $*
    fi
  done
  echo `fastidiousEaterHelper $*`
}

# Uncomment L50-L57 and comment L59-L66 for interactive use.
# echo "How many fries are on the plate?"
#
# while :
# do
#   read -p "Enter a nonnegative integer (RETURN to quit): " HITAGI
#   fastidiousEater $HITAGI
#   [ $? -eq 1 ] && echo "Invalid input."
# done

if [ $# -eq 0 ]; then
  echo "\"How many fries are on the plate?\""
fi

while [ $# -gt 0 ]; do
  fastidiousEater $1
  shift
done
