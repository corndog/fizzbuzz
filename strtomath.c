#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

double add(double left, double right) { return right + left; }
double addInverse(double left, double right) { return left - right; }
double multiply(double left, double right) { return right * left; }
double multiplyInverse(double left, double right_is_not_zero) {
  assert(right_is_not_zero);
  return left / right_is_not_zero;
}

double evaluate(char expr[]) {
  size_t length = strlen(expr); /* null term uncounted by strlen */
  double (**operations)(double, double) =
      malloc(length * sizeof(double (*)(double, double)));
  double* numbers = malloc(length * sizeof(double));
  double sum = 0;
  int i, m = 1, n = 0;
  operations[0] = &add; /* default */
  for (i = 0; i < length; i++) {
    switch (expr[i]) {
      /* No regex support. */
      case ' ': break;
      case '+': operations[m] = &add; m++; break;
      case '-': if (n == 0) {operations[0] = &addInverse; break;}
                operations[m] = &addInverse; m++; break;
      case '*': operations[m] = &multiply; m++; break;
      case '/': operations[m] = &multiplyInverse; m++; break;
      case '(': {
        int j = i + 1;
        int nested_parens = 0;
        char* inner_expr;
        while (!(expr[j] == ')' && nested_parens == 0)) {
          if (expr[j] == ')') nested_parens--;
          else if (expr[j] == '(') nested_parens++;
          j++;
        }
        /* get expression from ( to ) non-inclusive */
        inner_expr = malloc(j-i-1);
        strncpy(inner_expr, expr+i+1, j-i-1);
        if (operations[m-1] == &multiply ||
            operations[m-1] == &multiplyInverse) {
          numbers[n-1] = operations[m-1](numbers[n-1], evaluate(inner_expr));
          m--;  n--;
        }
        else numbers[n] = evaluate(inner_expr);
        free(inner_expr);
        n++; i = j;
        break;
      }
      default: {
        int j = i + 1;
        char* number;
        while (!(expr[j] == ' ' || expr[j] == '\0' || j >= length)) { j++; }
        /* get number from i to j inclusive */
        number = malloc(j-i+1);
        strncpy(number, expr+i, j-i+1);
        if (operations[m-1] == &multiply ||
            operations[m-1] == &multiplyInverse) {
          numbers[n-1] = operations[m-1](numbers[n-1], strtod(number, '\0'));
          m--; n--;
        }
        else numbers[n] = strtod(number, '\0');
        free(number);
        n++; i = j;
        break;
      }
    }
  }
  for (i = 0; i < n; i++) {
    sum = operations[i](sum, numbers[i]);
  }
  free(operations);
  free(numbers);
  return sum;
}

int main(int argc, char* argv[]) {
  double simplified;
  int i;
  if (argc < 2) return 0;
  for (i = 1; i < argc; i++) {
    simplified = evaluate(argv[i]);
    if (ceil(simplified) - simplified == 0) printf("%.0f\n", simplified);
    else if (floor(simplified) - simplified == 0) printf("%.0f\n", simplified);
    else printf("%f\n", simplified);
  }
  return 0;
}
